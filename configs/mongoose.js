let config = require('./configs');
const mongoose = require('mongoose');
module.exports = () => {
    mongoose.connect(config.mongodb + config.dbName, (err) => {
        if (err) console.log("error", err);
        else console.log("Mongodb Connected")
    });
}